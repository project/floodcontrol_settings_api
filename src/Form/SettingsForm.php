<?php

namespace Drupal\floodcontrol_settings_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\floodcontrol_settings_api\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'floodcontrol_settings_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Allow other modules to write settings.
    $form = \Drupal::moduleHandler()->invokeAll('floodcontrol_settings', $form);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $form_state->cleanValues();

    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      $this->config('floodcontrol_settings_api.settings')
        ->set($key, $value)
        ->save();
    }
  }

}
